package com.bignerdranch.android.locatr;

/**
 * Created by ergerodr on 8/9/2016.
 */
public class GalleryItem {
    private String mCaption;
    private String mCaptionID;
    private String mURL;
    private double mLat;
    private double mLon;

    @Override
    public String toString() {
        return mCaption;
    }

    public String getCaption() {
        return mCaption;
    }

    public void setCaption(String mCaption) {
        this.mCaption = mCaption;
    }

    public String getID() {
        return mCaptionID;
    }

    public void setID(String mCaptionID) {
        this.mCaptionID = mCaptionID;
    }

    public String getURL() {
        return mURL;
    }

    public void setURL(String mURL) {
        this.mURL = mURL;
    }

    public double getLon() {
        return mLon;
    }

    public void setLon(double mLon) {
        this.mLon = mLon;
    }

    public double getLat() {
        return mLat;
    }

    public void setLat(double mLat) {
        this.mLat = mLat;
    }
}
